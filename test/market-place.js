const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("MarketPlace", function () {
  it("Deploy ERC20 token contract", async function () {
    const [owner, account1, account2, account3, account4] = await ethers.getSigners();

    // Deploy ERC20 token contract.
    const ERC20Tokens = await hre.ethers.getContractFactory("Erc20WethToken");
    const erc20Tokens = await ERC20Tokens.deploy(1000000000000000);
    await erc20Tokens.deployed();

    // Deploy MarketPlace contract.
    const MarketPlace = await hre.ethers.getContractFactory("MarketPlace");
    const marketPlace = await MarketPlace.deploy(await erc20Tokens.address, await erc20Tokens.Owner(), 55, 100);
    await marketPlace.deployed();

    // Passing the erc20 allowance to the market place to transfer tokens between accounts.
    await erc20Tokens.approve(marketPlace.address, await erc20Tokens.balanceOf(await erc20Tokens.Owner()))
    const allowance = await erc20Tokens.allowance(await erc20Tokens.Owner(), marketPlace.address)

    // Testing that the market get the allowance.
    expect(allowance).to.equal(await erc20Tokens.balanceOf(await erc20Tokens.Owner()));

    // Deploy ERC 1155 contract.
    const ERC1155Tokens = await hre.ethers.getContractFactory("Erc1155Tokens");
    const erc1155Tokens = await ERC1155Tokens.deploy();
    await erc1155Tokens.deployed();

    // Register sale for the ERC 1155 token in Market.
    await marketPlace.register1155TokenForSale(erc1155Tokens.address, 1, 1000, 100, account1.address);

    // Approving the allowance of the 1155 tokens to marketPlace.
    await erc1155Tokens.setApprovalForAll(marketPlace.address, true)

    // Buy Weth for the account 1.
    await marketPlace.connect(account1).buyWethTokens(10000, {value: 10000})

    // Approve allowance to tranfer account 1 ERC20 tokens to Market.
    await erc20Tokens.connect(account1).approve(marketPlace.address, 10000)
    const allowance20 = await erc20Tokens.allowance(account1.address, marketPlace.address)

    // Testing that the market get the allowance.
    expect(allowance20).to.equal(await erc20Tokens.balanceOf(account1.address));
    
    // Buy 1155 asset for account 1.
    await marketPlace.connect(account1).buy1155Assets(1, 1)
    expect(await erc1155Tokens.balanceOf(account1.address, 1)).to.equal(1);


    // Deploy ERC 1155 contract.
    const ERC721Tokens = await hre.ethers.getContractFactory("ERC721Tokens");
    const erc721Tokens = await ERC721Tokens.deploy();
    await erc721Tokens.deployed();

     // Register sale for the ERC 721 token in Market.
     await marketPlace.register721TokenForSale(erc721Tokens.address, 1, 100, account1.address);

     // Approving the allowance of the 1155 tokens to marketPlace.
    await erc1155Tokens.setApprovalForAll(marketPlace.address, true)

    // Buy 721 asset for account 1.
    await marketPlace.connect(account1).buy721Assets(1)
    expect(await erc1155Tokens.balanceOf(account1.address, 1)).to.equal(1);
  });
});
