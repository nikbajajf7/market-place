//SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;


/// @dev: Imported required interfaces.
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721Receiver.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155Receiver.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

/// @title Market Place
/// @author Nikhil Bajaj
/// @notice Use for the ERC1155 and ERC721 tokens.
/// @custom:experimental This is an experimental contract.
contract MarketPlace {
    struct TokenSale1155 {
        address contractAddress;
        address payable tokenOwner;
        uint256 tokenId;
        uint256 tokensForSale;
        uint256 tokenPrice;
        address payable paymentAddress;
        bool tokenRegistered;
    }

    struct TokenSale721 {
        address contractAddress;
        address payable tokenOwner;
        uint256 tokenId;
        uint256 tokenPrice;
        address payable paymentAddress;
        bool tokenRegistered;
    }

    mapping (uint256 => TokenSale1155) public erc1155TokensForSale;
    mapping (uint256 => TokenSale721) public erc721TokensForSale;
    address erc20Address;
    address payable public erc20Owner;
    uint256 marketFeeNum;
    uint256 marketFeeDeno;
    uint256 public marketFeeCollection;

    modifier validateAddress(address _address, string memory _errorMessage) {
        require(_address != address(0), _errorMessage);
        _;
    }

    /// @notice Pass erc20 contract address as well as the owner of it.
    /// @param _erc20Address The number of rings from dendrochronological sample
    /// @param _erc20Owner The number of rings from dendrochronological sample
    constructor(address _erc20Address, address payable _erc20Owner, uint256 _marketFeeNum, uint256 _marketFeeDeno) public payable {
        erc20Address = _erc20Address;
        erc20Owner = _erc20Owner;
        marketFeeNum = _marketFeeNum;
        marketFeeDeno = _marketFeeDeno;
    }

    /// @notice Register the ERC1155 token for sale through this function.
    /// @param _contractAddress Contract address fo the ERC1155
    /// @param _tokenId Token id for which the sale has to register
    /// @param _tokensForSale Number of token available for sale
    /// @param _tokenPrice Each token price
    /// @param _paymentAddress Acceptable address for ERC20 tokens 
    function register1155TokenForSale(address _contractAddress, uint256 _tokenId, uint256 _tokensForSale, uint256 _tokenPrice, address payable _paymentAddress)
    external
    validateAddress(_contractAddress, "Contract address cannot be null address") {
        require(_tokensForSale > 0, "Tokens for sale should be greater than 0");
        require(IERC1155(_contractAddress).balanceOf(msg.sender, _tokenId) > _tokensForSale, "Insufficient tokens in your account for sale.");

        if(erc1155TokensForSale[_tokenId].tokenRegistered) {
            require(msg.sender == erc1155TokensForSale[_tokenId].tokenOwner, "Needs Owner permission to update the token sale status.");
        }

        erc1155TokensForSale[_tokenId] = TokenSale1155({
            contractAddress: _contractAddress,
            tokenOwner: payable (msg.sender),
            tokenId: _tokenId,
            tokensForSale: _tokensForSale,
            tokenPrice: _tokenPrice,
            paymentAddress: _paymentAddress,
            tokenRegistered: true
        });
    }

    /// @notice Register the ERC721 token for sale through this function.
    /// @param _contractAddress Contract address fo the ERC1155
    /// @param _tokenId Token id for which the sale has to register
    /// @param _tokenPrice Each token price
    /// @param _paymentAddress Acceptable address for ERC20 tokens 
    function register721TokenForSale(address _contractAddress, uint256 _tokenId, uint256 _tokenPrice, address payable _paymentAddress)
    external
    validateAddress(_contractAddress, "Contract address cannot be null address") {
        require(IERC721(_contractAddress).ownerOf(_tokenId) == msg.sender, "Owner can only register for sale");

        if(erc721TokensForSale[_tokenId].tokenRegistered) {
            require(msg.sender == erc721TokensForSale[_tokenId].tokenOwner, "Needs Owner permission to update the token sale status.");
        }

        erc721TokensForSale[_tokenId] = TokenSale721({
            contractAddress: _contractAddress,
            tokenOwner: payable (msg.sender),
            tokenId: _tokenId,
            tokenPrice: _tokenPrice,
            paymentAddress: _paymentAddress,
            tokenRegistered: true
        });
    }

    /// @notice Transfer ERC20 tokens.
    /// @param _from Address from which tokens has to deduct.
    /// @param value Number of tokens to transfer.
    /// @param _to Address to which tokens get credited
    function _paymentInWETH(address _from, uint256 value, address _to)
    internal
    validateAddress(_from, "payment address not valid.")
    returns(bool) {
        require(IERC20(erc20Address).balanceOf(_from) >= value, "Insufficient WETH tokens to buy assests.");
        uint256 contractAllowance = IERC20(erc20Address).allowance(_from, address(this));
        require(contractAllowance >= value, "Contract has no allowance to transfer funds.");
        bool response = IERC20(erc20Address).transferFrom(_from, _to, value);
        return response;
    }

    /// @notice Buy ERC20 tokens.
    /// @param amount Number of tokens
    function buyWethTokens(uint256 amount) external payable returns(bool) {
        require(payable(msg.sender).balance >= amount, "Insufficient ETH to buy the required WETH");
        bool success = payable(erc20Owner).send(amount);
        require(success, "Transaction failed.");
        bool transferWeth = IERC20(erc20Address).transferFrom(erc20Owner, msg.sender, amount);
        require(transferWeth, "Transfer of weth, failed.");
        return transferWeth;
    }

    /// @notice Payment in ETH.
    /// @param _to Account in which ETH has to transfer.
    /// @param _value Amount needs to transfer.
    function _paymentInETH(address payable _to, uint256 _value)
    internal
    returns(bool){
        (bool success,) = _to.call{value: _value}("");
        return success;
    }

    /// @notice Buy ERC1155 assests through this function.
    /// @param _tokenId ID of the token.
    /// @param _tokensRequested Number of token requested.
    function buy1155Assets(uint256 _tokenId, uint256 _tokensRequested) external payable {
        TokenSale1155 storage requestedToken = erc1155TokensForSale[_tokenId];
        require(requestedToken.tokenRegistered, "Token is not available for sale");
        require(requestedToken.tokensForSale >= _tokensRequested, "Insufficient tokens for sale");
        uint256 totalCost = requestedToken.tokenPrice * _tokensRequested;
        bool paymentSuccess;
        if(requestedToken.paymentAddress != address(0)) {
            paymentSuccess = _paymentInWETH(msg.sender, totalCost, requestedToken.paymentAddress);
        } else {
            paymentSuccess = _paymentInETH(requestedToken.tokenOwner, totalCost);
        }
        require(paymentSuccess, "payment failed.");
        IERC1155(requestedToken.contractAddress).safeTransferFrom(requestedToken.tokenOwner, msg.sender, requestedToken.tokenId, _tokensRequested, "");
        marketFeeCollection += totalCost*(marketFeeNum/marketFeeDeno);
        // IERC1155Receiver(requestedToken.contractAddress).onERC1155Received(msg.sender, requestedToken.tokenOwner, _tokenId, _tokensRequested, "");
    }

    /// @notice Buy ERC721 assests through this function.
    /// @param _tokenId ID of the token.
    function buy721Assets(uint256 _tokenId) external payable {
        TokenSale721 storage requestedToken = erc721TokensForSale[_tokenId];
        require(requestedToken.tokenRegistered, "Token is not available for sale");
        uint256 totalCost = requestedToken.tokenPrice;
        bool paymentSuccess;
        if(requestedToken.paymentAddress != address(0)) {
            paymentSuccess = _paymentInWETH(msg.sender, totalCost, requestedToken.paymentAddress);
        } else {
            paymentSuccess = _paymentInETH(requestedToken.tokenOwner, totalCost);
        }
        require(paymentSuccess, "payment failed.");
        IERC721(requestedToken.contractAddress).safeTransferFrom(requestedToken.tokenOwner, msg.sender, requestedToken.tokenId);
        marketFeeCollection += totalCost*(marketFeeNum/marketFeeDeno);
        // IERC721Receiver(requestedToken.contractAddress).onERC721Received(msg.sender, requestedToken.tokenOwner, _tokenId, "");
    }
}
