//SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Erc20WethToken is ERC20 {
    address public Owner;
    constructor(uint256 initialSupply) public ERC20("ETH", "weth") {
        Owner = msg.sender;
        _mint(msg.sender, initialSupply);
    }
}
