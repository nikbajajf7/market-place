// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";


contract ERC721Tokens is ERC721 {
    uint256 public constant Gold = 1;

    constructor() public ERC721("GameItem", "ITM") {
        _mint(msg.sender, Gold);
    }
}