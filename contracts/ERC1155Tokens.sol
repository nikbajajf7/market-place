//SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";

contract Erc1155Tokens is ERC1155 {
    uint256 public constant Gold = 1;
    uint256 public constant Silver = 1;

   constructor() public ERC1155("") {
        _mint(msg.sender, Gold, 10**18, "");
        _mint(msg.sender, Silver, 10**18, "");
    }
}
